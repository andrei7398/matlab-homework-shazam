function [person_id] = ecg_function(input_signal)
  
  for i = 0:90
    %int2str(i);
    if i < 10
      person = "Person_0" + int2str(i) + "\rec_1m.mat";
      %person = "Person_0%s\rec_1m.mat";
    else
      person = "Person_" + int2str(i) + "\rec_1m.mat";
      %person = "Person_%s\rec_1m.mat";
    endif  
    load(person,'var');

    Y = fft(var(2,:));
    
    Fs = 500;           % Sampling frequency
    T = 1/Fs ;          % Sampling period
    L = 5000;           % Length of signal
    t = (0 : L-1)*T ;   % Time vector
    f = Fs*(0 : (L/2))/L ;

    P2 = abs(Y/L);
    P1 = P2(1:L / 2+1);
    P1(2 : end-1) = 2*P1(2 : end-1);
  
    [V,I] = sort(P1,'descend');
 
    for j = 1:2:8
      VEC1(j) = V(j);
      VEC1(j+1) = f(I(j));
    endfor

  endfor
  
  Y = fft(input_signal);
  Fs = 500;           % Sampling frequency
  T = 1/Fs ;          % Sampling period
  L = 5000;           % Length of signal
  t = (0 : L-1)*T ;   % Time vector
  f = Fs*(0 : (L/2))/L ;
  
  P2 = abs(Y/L);
  P1 = P2(1:L/2+1);
  P1(2 : end-1) = 2*P1(2 : end-1);
  
  [T,J] = sort(P1,'descend');
  
  for j = 1:2:8
     VEC2(j) = T(j);
     VEC2(j+1) = f(J(j));
  endfor
  
  %VEC1  = rand(1, 8);
  %VEC2 = rand(1, 8);
  %DIST  = sqrt(sum((VEC - VEC2).^2));
  
  DIST = norm(VEC1(1,:)-VEC2, 2);
  
  PRS = 1;
  M = 1;
  
  for i = 1:90
    if (norm(VEC1(i,:)-VEC2, 2) < M)
      M = norm(VEC1(i,:)-VEC2, 2);
      PRS = i;
    endif
  endfor
  
  person_id = PRS;
  
endfunction
