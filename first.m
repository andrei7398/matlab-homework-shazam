function first
  %graphics_toolkit('gnuplot');
  
  k = 20;
  
  t = -10:0.01:10;
  
  u = cos(100*t+pi/3);
  h = exp(-k*t);
  
  y = conv(u,h);

  plot(y)

endfunction
