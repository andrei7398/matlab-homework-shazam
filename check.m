percent = 0;
for i = 1:90
    s1 = 'Person_';
    
    if(i<10)
      s1 = [s1 '0'];
    end
    
    s = [s1 int2str(i) '/rec_' 'lm.mat'];
    
    load(s, 'val');
    
    x = ecg_function(val(2,:),0);
    
    if(x==i)
      percent = percent + 1;
    end
    
endfor
disp(percent/90 * 100 + "%");